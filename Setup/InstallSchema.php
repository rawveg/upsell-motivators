<?php


namespace Rawveg\UpsellMotivator\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table_rawveg_upsellmotivator_message = $setup->getConnection()->newTable($setup->getTable('rawveg_upsellmotivator_message'));

        $table_rawveg_upsellmotivator_message->addColumn(
            'message_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,),
            'Entity ID'
        );

        $table_rawveg_upsellmotivator_message->addColumn(
            'message',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'message'
        );

        $table_rawveg_upsellmotivator_message->addColumn(
          'subtotal',
          \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
          '10,2',
          ['nullable' => false],
          'Subtotal Value'
        );

        $table_rawveg_upsellmotivator_message->addColumn(
          'from_date',
          \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
          '10,2',
          ['nullable' => true],
          'Message from Date'
        );

        $table_rawveg_upsellmotivator_message->addColumn(
          'to_date',
          \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
          '10,2',
          ['nullable' => true],
          'Message to Date'
        );

        $table_rawveg_upsellmotivator_message->addColumn(
          'created_at',
          \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
          '10,2',
          ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
          'Created At'
        );

        $setup->getConnection()->createTable($table_rawveg_upsellmotivator_message);

        $setup->endSetup();
    }
}
