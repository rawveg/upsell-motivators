<?php
namespace Rawveg\UpsellMotivator\Helper;

use Magento\Framework\App\Helper\Context;
use Rawveg\UpsellMotivator\Model\ResourceModel\Message\Collection;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Helper\Cart as CartHelper;

/**
 * Class Data
 * @package Pillbox\TrustpilotWidgets\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

  protected $model;
  protected $cart;
  protected $carthelper;

  /**
   * Updated constructor
   * @param Context $context
   * @param Collection $model
   * @param Cart    $cart
   */
  public function __construct(Context $context, Collection $model, Cart $cart, CartHelper $helper)
  {
    $this->model = $model;
    $this->cart = $cart;
    $this->carthelper = $helper;
    parent::__construct($context);
  }

  /**
   * Gets Valid Message
   * @return string|bool
   */
  public function getMessage()
  {
    $retVal = false;
    $subtotal = $this->getCartSubtotal();
    $messages = $this->getMessageCollection();
    foreach($messages as $message)
    {
      if($subtotal < $message->getSubtotal())
      {
        $diff = $message->getSubtotal() - $subtotal;
        $retVal = sprintf($message->getMessage(), $diff);
        break;
      }
    }
    return $retVal;
  }

  /**
   * Get Cart Subtotal
   * @var float
   */
  public function getCartSubtotal()
  {
    return $this->cart->getQuote()->getSubtotal();
  }

  /**
   * Get Message Collection
   * @var \Rawveg\UpsellMotivator\Model\ResourceModel\Message\Collection
   */
  public function getMessageCollection()
  {
    $now = new \DateTime();
    return $this->model->addFieldToFilter(array('from_date','from_date'), array(['lteq'=>$now->format('Y-m-d H:i:s')],['null'=>true]))
                       ->addFieldToFilter(array('to_date','to_date'), array(['gteq'=>$now->format('Y-m-d H:i:s')],['null'=>true]))
                       ->addFieldToFilter('subtotal', ['gt' => $this->getCartSubtotal()])
                       ->addOrder('main_table.subtotal', 'ASC')
                       ->setPageSize(1)
                       ->setCurPage(1)
                       ->load();
  }

  /**
   * Checks to see if the module is enabled
   * @return boolean Enabled State
   */
  public function isEnabled()
  {
    $retVal = false;
    $isMessage = $this->getMessage();
    if($isMessage !== false && $this->carthelper->getItemsCount() !== 0)
    {
      $retVal = ($this->getConfig('upsellmotivator/general/enable')  == 1) ? true : false;
    }
    return $retVal;
  }

  /**
   * Gets config value from core_config_data
   * @param  string $config_path Path to Configuration value
   * @return mixed              Result
   */
  public function getConfig($config_path)
  {
    return $this->scopeConfig->getValue($config_path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
  }

}
