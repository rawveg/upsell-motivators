# Upsell Motivators

Upsell motivators fills a simple need, to encourage customers to spend a little more by advising them of various conditions, such as 'Spend £12.30 more to qualify for free shipping'. 

To install using composer:-

    composer require rawveg/module-upsellmotivator
    bin/magento setup:upgrade
    bin/magento cache:flush 
    bin/magento cache:clean

Setup:-

1.  Enable the module by visiting: **Stores > Configuration > Rawveg > Upsell Motivators > Enabled** <br /><br />![Enabling the Module](https://s19.postimg.cc/b8n6wxdbn/Screen_Shot_2018-05-28_at_22.23.45.png)
2.  Messages can be added by visiting Rawveg > Upsell Motivators <br /><br />![Upsell Motivators](https://s19.postimg.cc/vss0vedn7/Screen_Shot_2018-05-28_at_22.24.12.png)
3.  Click on *Add Motivator* to add a new message <br /><br />![Screen_Shot_2018-05-28_at_22.24.55.png](https://s19.postimg.cc/xkkzqbk5f/Screen_Shot_2018-05-28_at_22.24.55.png)
4.  Add a subtotal value and a message. The message should be in a format that is understandable by sprintf, so replace the variable amount by the string *%s*. Optionally add start and finish dates for the message.<br /><br />![Screen_Shot_2018-05-28_at_22.26.07.png](https://s19.postimg.cc/cnorlntub/Screen_Shot_2018-05-28_at_22.26.07.png)
5.  Click *Save Motivator* to save your message.<br /><br />![Screen_Shot_2018-05-28_at_22.27.04.png](https://s19.postimg.cc/wv27dy46b/Screen_Shot_2018-05-28_at_22.27.04.png)
5.  The messages are implemented as Widgets, which can be placed anywhere you like on the site. Add a widget by visiting *Content > Widgets*, click *Add Widget*.
6.  In *Type* select *Upsell Motivators*<br /><br />![Screen_Shot_2018-05-28_at_22.28.42.png](https://s19.postimg.cc/xxcdwh7k3/Screen_Shot_2018-05-28_at_22.28.42.png)
7.  Select your Theme, and click *continue*<br /><br />![Screen_Shot_2018-05-28_at_22.29.03.png](https://s19.postimg.cc/prubybblf/Screen_Shot_2018-05-28_at_22.29.03.png)
8.  Select your Store, Sort Order and Layout Updates.<br /><br />![Screen_Shot_2018-05-28_at_22.30.31.png](https://s19.postimg.cc/e2qcae2n7/Screen_Shot_2018-05-28_at_22.30.31.png)
9.  Click Widget Options and choose the width and height.<br /><br />![Screen_Shot_2018-05-28_at_22.30.55.png](https://s19.postimg.cc/ya3s2naeb/Screen_Shot_2018-05-28_at_22.30.55.png)
10.  Click *Save*

Now you're all set to view the results!<br /><br /><br />
![Screen_Shot_2018-05-28_at_23.06.01.png](https://s19.postimg.cc/4w81n27oj/Screen_Shot_2018-05-28_at_23.06.01.png)

