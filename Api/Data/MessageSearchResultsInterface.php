<?php


namespace Rawveg\UpsellMotivator\Api\Data;

interface MessageSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get Message list.
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface[]
     */
    public function getItems();

    /**
     * Set message list.
     * @param \Rawveg\UpsellMotivator\Api\Data\MessageInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
