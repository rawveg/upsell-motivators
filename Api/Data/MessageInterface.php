<?php


namespace Rawveg\UpsellMotivator\Api\Data;

interface MessageInterface
{

    const MESSAGE = 'message';
    const MESSAGE_ID = 'message_id';
    const SUBTOTAL_VALUE = 'subtotal';
    const FROM_DATE = 'from_date';
    const TO_DATE = 'to_date';


    /**
     * Get message_id
     * @return string|null
     */
    public function getMessageId();

    /**
     * Set message_id
     * @param string $message_id
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface
     */
    public function setMessageId($messageId);

    /**
     * Get message
     * @return string|null
     */
    public function getMessage();

    /**
     * Set message
     * @param string $message
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface
     */
    public function setMessage($message);

    /**
     * Set Subtotal Value
     * @param float $subtotalValue
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface
     */
    public function setSubtotalValue($subtotalValue);

    /**
     * Get Subtotal Value
     * @return float|null
     */
    public function getSubtotalValue();

    /**
     * Set From Date
     * @param string $fromDate
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface
     */
    public function setFromDate($fromDate);

    /**
     * Get From Date
     * @return string|null
     */
    public function getFromDate();

    /**
     * Set To Date
     * @param string $toDate
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface
     */
    public function setToDate($toDate);

    /**
     * Get To Date
     * @return string|null
     */
    public function getToDate();
}
