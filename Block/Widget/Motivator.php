<?php
namespace Rawveg\UpsellMotivator\Block\Widget;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Rawveg\UpsellMotivator\Helper\Data;

class Motivator extends Template implements BlockInterface {

  protected $_template = "widget/motivator_block.phtml";

  protected $helper;

  /**
   * Alternative contructor
   * @param Context $context [description]
   * @param Data    $helper  [description]
   */
  public function __construct(Context $context, Data $helper)
  {
    $this->helper = $helper;
    parent::__construct($context);
  }

  /**
   * getMessage
   * @return string
   */
  public function getMessage()
  {
    return $this->helper->getMessage();
  }

  public function getCartSubtotal()
  {
    return $this->helper->getCartSubtotal();
  }

  /**
   * isEnabled
   * @return boolean
   */
  public function isEnabled()
  {
    return $this->helper->isEnabled();
  }

}
