<?php


namespace Rawveg\UpsellMotivator\Model;

use Rawveg\UpsellMotivator\Api\Data\MessageInterface;

class Message extends \Magento\Framework\Model\AbstractModel implements MessageInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Rawveg\UpsellMotivator\Model\ResourceModel\Message');
    }

    /**
     * Get message_id
     * @return string
     */
    public function getMessageId()
    {
        return $this->getData(self::MESSAGE_ID);
    }

    /**
     * Set message_id
     * @param string $messageId
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface
     */
    public function setMessageId($messageId)
    {
        return $this->setData(self::MESSAGE_ID, $messageId);
    }

    /**
     * Get message
     * @return string
     */
    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }

    /**
     * Set message
     * @param string $message
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface
     */
    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /**
     * Set Subtotal Value
     * @param float $subtotalValue
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface
     */
    public function setSubtotalValue($subtotalValue)
    {
      return $this->setData(self::SUBTOTAL_VALUE, $subTotalValue);
    }

    /**
     * Get Subtotal Value
     * @return float|null
     */
    public function getSubtotalValue()
    {
      return $this->getData(self::SUBTOTAL_VALUE);
    }

    /**
     * Set From Date
     * @param string $fromDate
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface
     */
    public function setFromDate($fromDate)
    {
      return $this->setData(self::FROM_DATE, $fromDate);
    }

    /**
     * Get From Date
     * @return string|null
     */
    public function getFromDate()
    {
      return $this->getData(self::FROM_DATE);
    }

    /**
     * Set To Date
     * @param string $toDate
     * @return \Rawveg\UpsellMotivator\Api\Data\MessageInterface
     */
    public function setToDate($toDate)
    {
      return $this->setData(self::TO_DATE, $toDate);
    }

    /**
     * Get To Date
     * @return string|null
     */
    public function getToDate()
    {
      return $this->getData(self::TO_DATE);
    }
}
