<?php


namespace Rawveg\UpsellMotivator\Model\ResourceModel;

class Message extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('rawveg_upsellmotivator_message', 'message_id');
    }
}
