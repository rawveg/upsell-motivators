<?php


namespace Rawveg\UpsellMotivator\Model\ResourceModel\Message;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Rawveg\UpsellMotivator\Model\Message',
            'Rawveg\UpsellMotivator\Model\ResourceModel\Message'
        );
    }
}
